from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import ToDoItem


# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    
    template = loader.get_template('todolist/index.html')
    context = {
        'todoitem_list': todoitem_list
    }

    # return HttpResponse(template.render(context, request))

    # You can also render a template using this way
    return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response % todoitem_id)